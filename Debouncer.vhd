----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.12.2018 18:05:00
-- Design Name: 
-- Module Name: Debouncer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--use ieee.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

--CAMBIADOS RESET
entity Debouncer is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           btn_in : in STD_LOGIC;
           btn_out : out STD_LOGIC);
end Debouncer;

architecture Behavioral of Debouncer is
    constant CNT_SIZE : integer := 20; --Calculado para 100MHz de reloj, que es el valor del reloj de la FPGA
    signal btn_prev   : std_logic := '0';
    signal counter    : unsigned(CNT_SIZE downto 0) := (others => '0');

begin
    process(clk)
    begin
    if(reset='0') then
               counter<= (others => '0');
               btn_prev<='0';
    else 
	   if (clk'event and clk='1') then
	       if (btn_prev xor btn_in) = '1' then
		      counter <= (others => '0');
		      btn_prev <= btn_in;
           else
                if (counter(CNT_SIZE) = '0') then
                    counter <= counter + 1;
                else
                    btn_out <= btn_prev;
                end if;
	       end if;
	   end if;
	end if;
    end process;
end Behavioral;
