library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity OUT_PASS is
    Port (
    STATE: IN integer range 5 downto 0;
    LED: OUT std_logic; 
    LEDTEST: out std_logic_vector (4 downto 0));
end OUT_PASS;

architecture Behavioral of OUT_PASS is
begin
    process(STATE)
        begin
        case(state) is
            when 0 => LEDTEST(0) <= '1';
                        LEDTEST(1) <= '0';
                        LEDTEST(2) <= '0';
                        LEDTEST(3) <= '0';
                        LEDTEST(4) <= '0';
                        LED <= '0';
            when 1 => LEDTEST(1) <= '1';
                        LEDTEST(0) <= '0';
                        LEDTEST(2) <= '0';
                        LEDTEST(3) <= '0';
                        LEDTEST(4) <= '0';
                        LED <= '0';
            when 2 => LEDTEST(2) <= '1';
                        LEDTEST(0) <= '0';
                        LEDTEST(1) <= '0';
                        LEDTEST(3) <= '0';
                        LEDTEST(4) <= '0';
                        LED <= '0';
            when 3 => LEDTEST(3) <= '1';
                        LEDTEST(0) <= '0';
                        LEDTEST(2) <= '0';
                        LEDTEST(1) <= '0';
                        LEDTEST(4) <= '0';
                        LED <= '0';
            when 4 => LEDTEST(4) <= '1';
                        LEDTEST(0) <= '0';
                        LEDTEST(2) <= '0';
                        LEDTEST(3) <= '0';
                        LEDTEST(1) <= '0';
                        LED <= '0';
            
            when 5 => LED<='1';
                        LEDTEST(0) <= '0';
                        LEDTEST(2) <= '0';
                        LEDTEST(3) <= '0';
                        LEDTEST(1) <= '0';
                        LEDTEST(4) <= '0';
            when others => LED<='0'; 
        end case;
    end process;
end Behavioral;
