library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity OUT_PASS_tb is
--  Port ( );
end OUT_PASS_tb;

architecture Behavioral of OUT_PASS_tb is
    component OUT_PASS is
        port ( STATE: IN integer;
            LED: OUT std_logic );
    end component;
    
signal led: std_logic;
signal stage: integer;
    
begin
    uut: OUT_PASS PORT MAP(
    LED=>led,
    STATE=>stage
    );
    tb: process
    begin
        stage <= 0;
        wait for 10 ns;
        stage <= 1;
        wait for 10ns;
        stage <= 2;
        wait for 10ns;
        stage <= 3;
        wait for 10ns;
        stage <= 4;
        wait for 10 ns;
        stage <= 5;
        wait for 10 ns;
        stage <= 0;
    end process;
end Behavioral;
