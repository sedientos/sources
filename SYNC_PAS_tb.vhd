library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SYNC_PAS_tb is
end SYNC_PAS_tb;

architecture Behavioral of SYNC_PAS_tb is
component SYNC_PAS is
    PORT(
            clk: IN std_logic;
            next_state: IN integer;
            reset: IN std_logic;
            state: OUT integer
         );
end component;
    
SIGNAL clk: std_logic:='0';
SIGNAL next_state:  integer:=0;
SIGNAL reset: std_logic:='0';
SIGNAL state: integer;

BEGIN
   uut: SYNC_PAS PORT MAP(
   clk => clk,
   next_state => next_state,
   reset => reset,
   state => state
    );
   clk<=not clk after 5ns;
   
   PROCESS 
   BEGIN
    wait for 10 ns;
    next_state <= next_state + 1;
    wait for 10 ns;
    next_state <= next_state + 1;
    wait for 10 ns;
    next_state <= next_state + 1;
    wait for 18 ns;
    reset <= '1';
    wait for 20 ns;
    reset <= '0';
    next_state <= next_state + 1;
    wait for 10 ns; 
   END PROCESS;
end Behavioral;
