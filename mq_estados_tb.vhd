library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mq_estados_tb is
end mq_estados_tb;

architecture Behavioral of mq_estados_tb is

component maq_estados
Port(   BOTON: in STD_LOGIC_VECTOR(4 downto 0);
        LED: out std_logic;
        RESET: in std_logic;
        CLK: in std_logic;
        ESTADO0: out integer range 5 downto 0;
        ESTADO: out integer range 5 downto 0);
end component;

signal boton: std_logic_vector(4 downto 0);
signal led: std_logic:='0';
signal reset: std_logic:='0';
signal clk: std_logic:='0';
signal estado: integer range 5 downto 0 :=0;
signal estado0: integer range 5 downto 0 :=0;

begin
utt: maq_estados PORT MAP(
BOTON => boton,
LED => led,
RESET => reset,
CLK => clk,
ESTADO => estado,
ESTADO0 => estado0);

clk <= not clk after 5ns;
tb: process
begin
    boton <= "00000";
    wait for 10 ns;
    boton <= "00001";
    wait for 10 ns;
    boton <= "00010";
    wait for 10 ns;
    boton <= "00100";
    reset <= '1';
    wait for 10 ns;
    boton <= "01000";
    wait for 10 ns;
    boton <= "10000";
    reset <= '0';
    wait for 50 ns;
     boton <= "00000";
       wait for 10 ns;
       boton <= "00001";
       wait for 10 ns;
       boton <= "00010";
       wait for 10 ns;
       boton <= "00100";
       wait for 10 ns;
        boton <= "01000";
       wait for 10 ns;
       boton <= "10000";
       wait for 20 ns;
       reset <='1';
       wait for 10 ns;
end process;
end Behavioral;
