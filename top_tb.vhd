----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08.12.2018 13:51:30
-- Design Name: 
-- Module Name: top_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top_tb is
--  Port ( );
end top_tb;

architecture Behavioral of top_tb is

component top is
    Port ( btn : in STD_LOGIC_VECTOR (4 downto 0);
           light : out STD_LOGIC;
           clk : in STD_LOGIC;
           reset : in STD_LOGIC);
end component;

signal but: std_logic_vector (4 downto 0);
signal luz: std_logic;
signal clock: std_logic:='0';
signal rst: std_logic:='0';

begin

uut: top port map(but,luz,clock,rst);

clock<=not clock after 5 ns;

process
begin
but<="00000";
wait for 25 ms;
but<="00001";
wait for 25 ms;
but<="00010";
wait for 25 ms;
but<="00100";
wait for 25 ms;
but<="01000";
wait for 25 ms;
but<="10000";
wait for 50 ms;
rst<='1';
wait for 25 ms;
rst<='0';
wait for 25 ms;
but<="00000";
wait for 25 ms;
but<="00001";
wait for 25 ms;
but<="00010";
wait for 25 ms;
but<="00100";
wait for 25 ms;
but<="10000";
wait;

end process;

end Behavioral;
