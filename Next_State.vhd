----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.12.2018 18:05:00
-- Design Name: 
-- Module Name: Next_State - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Next_State is
    Port ( 
        btn : in STD_LOGIC_VECTOR (4 downto 0);
        state_in: in integer range 5 downto 0;
        state_out: out integer range 5 downto 0
        );
end Next_State;

architecture Behavioral of Next_State is
begin
next_state: process(btn)
begin
state_out<=state_in;

IF(state_in=0) THEN
    IF(btn(0) = '1') THEN
        state_out <=1;
    ELSIF (btn(1)='1' or btn(2)='1' or btn(3)='1' or btn(4)='1') then
        state_out<=0;
    else
        state_out <= state_in; 
    END IF;
END IF;

IF(state_in=1) THEN
    IF(btn(1) = '1') THEN
        state_out <=2;
    ELSIF (btn(2)='1' or btn(3)='1' or btn(4)='1') then
        state_out<=0;
    else
        state_out <= state_in; 
    END IF;
END IF;

IF(state_in=2) THEN
    IF(btn(2) = '1') THEN
        state_out <=3;
    ELSIF (btn(0)='1' or btn(3)='1' or btn(4)='1') then
        state_out<=0;
    else
        state_out <= state_in; 
    END IF;
END IF;

IF(state_in=3) THEN
    IF(btn(3) = '1') THEN
        state_out <=4;
    ELSIF (btn(1)='1' or btn(0)='1' or btn(4)='1') then
        state_out<=0;
    else
        state_out <= state_in; 
    END IF;
END IF;

IF(state_in=4) THEN
    IF(btn(4) = '1') THEN
        state_out <=5;
    ELSIF (btn(1)='1' or btn(2)='1' or btn(0)='1') then
        state_out<=0;
    else
        state_out <= state_in; 
    END IF;
END IF;
--    IF (btn(0) = '1' AND state_in = 0) THEN
--                state_out <= 1;
--    END IF;

--    IF (btn(1) = '1' AND state_in = 1) THEN
--                state_out <= 2;
--    END IF;
     

--    IF (btn(2) = '1' AND state_in = 2) THEN
--                state_out <= 3;
--    END IF;
    

--    IF (btn(3) = '1' AND state_in = 3) THEN
--        state_out <= 4;
--    END IF;


--    IF (btn(4) = '1' AND state_in = 4) THEN
--        state_out <= 5;
--    END IF;
  
END PROCESS; 

end Behavioral;
