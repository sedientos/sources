----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.12.2018 18:09:56
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top is
    Port ( btn : in STD_LOGIC_VECTOR (4 downto 0);
           light : out STD_LOGIC;
           clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           ledtest: out STD_LOGIC_VECTOR (4 downto 0));
           reset : in STD_LOGIC);
end top;

architecture Behavioral of top is

component sincronizador is
 Port (
        sync_in: IN STD_LOGIC;
        clk: IN STD_LOGIC;
        sync_out: OUT STD_LOGIC
 );
end component;

component Debouncer is
    Port ( 
        clk : in STD_LOGIC;
        reset : in STD_LOGIC;
        btn_in : in STD_LOGIC;
        btn_out : out STD_LOGIC);
end component;

component maq_estados is
  Port (
       BOTON: in STD_LOGIC_VECTOR(4 downto 0);
       LED: out std_logic;
       RESET: in std_logic;
       CLK: in std_logic;
       LEDTEST: out std_logic_vector (4 downto 0)
       CLK: in std_logic
   );
end component;

signal btn_sync: std_logic_vector (4 downto 0);
signal btn_deb: std_logic_vector (4 downto 0);


begin

instances: FOR i in 0 to 4 generate
begin
    sync: Sincronizador Port map(
         sync_in=>btn(i),
         clk=> clk,
         sync_out=> btn_sync(i)
     );
     
     debounc: Debouncer Port map(
             clk => clk,
             reset => reset,
             btn_in => btn_sync(i),
             btn_out => btn_deb(i)
      );
          
end generate;

maquina_estados: maq_estados port map(
    BOTON => btn_deb,
    LED => light,
    RESET => reset,
    CLK => clk,
    LEDTEST => ledtest
    CLK => clk
   );

end Behavioral;
