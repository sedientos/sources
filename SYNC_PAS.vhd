LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.ALL;
USE ieee.std_logic_unsigned.ALL;

--CAMBIO RESET
ENTITY  sync_pas IS
PORT(
        clk: IN std_logic;
        next_state: IN integer range 5 downto 0;
        reset: IN std_logic;
        state: OUT integer range 5 downto 0 :=0
     );
END ENTITY;


ARCHITECTURE behavioral OF sync_pas IS
BEGIN
    SYNC_PAS: PROCESS (clk)
    BEGIN
        IF rising_edge(clk) THEN
            IF (reset = '0') THEN
                state <= 0;
                ELSE
                state <= next_state;
            END IF;
        END IF;
    END PROCESS;
END ARCHITECTURE;