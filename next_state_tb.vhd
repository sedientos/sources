----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08.12.2018 11:30:41
-- Design Name: 
-- Module Name: next_state_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity next_state_tb is
--  Port ( );
end next_state_tb;

architecture Behavioral of next_state_tb is

component Next_State is
    Port ( 
        btn : in STD_LOGIC_VECTOR (4 downto 0);
        state_in: in integer range 5 downto 0;
        state_out: out integer range 5 downto 0
        );
end component;

signal boton: STD_LOGIC_VECTOR (4 downto 0);
signal estado: integer range 5 to 0;
signal siguiente: integer range 5 to 0;

TYPE vtest is record
    boton: STD_LOGIC_VECTOR (4 downto 0);
    estado: integer range 5 to 0;
    siguiente: integer range 5 to 0;
END RECORD;

TYPE vtest_vector IS ARRAY (natural RANGE <>) OF vtest;

CONSTANT test: vtest_vector := (
(boton => "00000",estado=>0, siguiente =>0),
(boton => "00001",estado=>0, siguiente =>1),
(boton => "00010",estado=>0, siguiente =>0),
(boton => "00100",estado=>0, siguiente =>0),
(boton => "01000",estado=>0, siguiente =>0),
(boton => "10000",estado=>0, siguiente =>0),

(boton => "00000",estado=>1, siguiente =>0),
(boton => "00001",estado=>1, siguiente =>0),
(boton => "00010",estado=>1, siguiente =>2),
(boton => "00100",estado=>1, siguiente =>0),
(boton => "01000",estado=>1, siguiente =>0),
(boton => "10000",estado=>1, siguiente =>0),

(boton => "00000",estado=>2, siguiente =>0),
(boton => "00001",estado=>2, siguiente =>0),
(boton => "00010",estado=>2, siguiente =>0),
(boton => "00100",estado=>2, siguiente =>3),
(boton => "01000",estado=>2, siguiente =>0),
(boton => "10000",estado=>2, siguiente =>0),

(boton => "00000",estado=>3, siguiente =>0),
(boton => "00001",estado=>3, siguiente =>0),
(boton => "00010",estado=>3, siguiente =>0),
(boton => "00100",estado=>3, siguiente =>0),
(boton => "01000",estado=>3, siguiente =>4),
(boton => "10000",estado=>3, siguiente =>0),

(boton => "00000",estado=>4, siguiente =>0),
(boton => "00001",estado=>4, siguiente =>0),
(boton => "00010",estado=>4, siguiente =>0),
(boton => "00100",estado=>4, siguiente =>0),
(boton => "01000",estado=>4, siguiente =>0),
(boton => "10000",estado=>4, siguiente =>5),

(boton => "00000",estado=>5, siguiente =>5),
(boton => "00001",estado=>5, siguiente =>5),
(boton => "00010",estado=>5, siguiente =>5),
(boton => "00100",estado=>5, siguiente =>5),
(boton => "01000",estado=>5, siguiente =>5),
(boton => "10000",estado=>5, siguiente =>5)
);

begin

uut: Next_state PORT MAP (
        btn=>boton,
        state_in=>estado,
        state_out=>siguiente
     );
    
process
begin

for i in 0 to test'high loop
    boton<=test(i).boton;
    estado<=test(i).estado;
    wait for 20 ns;
    assert siguiente=test(i).siguiente
        REPORT "Salida incorrecta."
        SEVERITY FAILURE;
    end loop;
    
    assert  false
        REPORT "Simulación finalizada. Test superado."
        SEVERITY FAILURE;

end process;

end Behavioral;
