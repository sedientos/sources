----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.12.2018 18:05:00
-- Design Name: 
-- Module Name: Sincronizador - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sincronizador is
 Port (
 sync_in: IN STD_LOGIC;
 clk: IN STD_LOGIC;
 sync_out: OUT STD_LOGIC
 );
end sincronizador;

architecture Behavioral of sincronizador is

constant SYNC_STAGES : integer := 3;
constant INIT : std_logic := '0';

signal sreg : std_logic_vector(SYNC_STAGES-1 downto 0) := (others => INIT);

--signal sreg_pipe : std_logic_vector(PIPELINE_STAGES-1 downto 0) := (others => INIT);

begin
-- process(clk)
-- begin
-- if(clk'event and clk='1')then
-- sreg <= sreg(SYNC_STAGES-2 downto 0) & sync_in;
-- end if;
-- end process;
 
-- one_pipeline: process(clk)
-- begin
-- if(clk'event and clk='1') then
-- sync_out <= sreg(SYNC_STAGES-1);
-- end if;
-- end process;
process(clk)
begin
if(clk'event and clk='1') then
    sync_out <= sync_in;
 end if;
 end process;
 end Behavioral;

