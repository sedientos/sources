library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity maq_estados is
  Port (BOTON: in STD_LOGIC_VECTOR(4 downto 0);
       LED: out std_logic;
       RESET: in std_logic;
       CLK: in std_logic;
       LEDTEST: out std_logic_vector (4 downto 0)
   );
end maq_estados;

architecture Behavioral of maq_estados is
signal next_sync: integer range 5 downto 0;
signal sync_out: integer range 5 downto 0;

component Next_State
    Port(btn : in STD_LOGIC_VECTOR (4 downto 0);
        state_in: in integer range 5 downto 0;
        state_out: out integer range 5 downto 0
        );
end component;

component sync_pas
PORT(
        clk: IN std_logic;
        next_state: IN integer range 5 downto 0;
        reset: IN std_logic;
        state: OUT integer range 5 downto 0
     );
end component;

component OUT_PASS
    Port (
        STATE: IN integer range 5 downto 0;
        LED: OUT std_logic;
        LEDTEST: out std_logic_vector (4 downto 0)
        );
end component;

begin

Inst_next_state: Next_State PORT MAP(
    btn => BOTON,
    state_in => sync_out,
    state_out => next_sync);

Inst_sync_pas: sync_pas PORT MAP(
    clk => CLK,
    next_state => next_sync,
    reset => RESET,
    state => sync_out);

Inst_out_pas: OUT_PASS PORT MAP(
    STATE => sync_out,
    LED => LED,
    LEDTEST => LEDTEST);

end Behavioral;
